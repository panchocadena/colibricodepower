Add custom block Colibricode Power, Drupal 8 module
=================================================
Description: adds a custom block with text and image, displays a link to colibricode.com
is ment to be as a site signature and be placed on footer region.



Installation with command line
==============================

on <drupal8-root> directory, in the command line run in order the following 2 commands:

    mkdir -p modules/custom

    git -C modules/custom clone https://bitbucket.org/panchocadena/colibricodepower


  enable module

  on block UI settings place block on desired region.



To use a different image:
=========================
  Current image is for a dark background, 129x45 px.

  Change image:  Upload your image in {modulename}/image directory, then you have two optinos:
    1.- rename new file as logo-colibricode-white.png, or
    2.- update code in {modulename}/src/Plugin/Block/colibricodepowerBlock.php, line 32 with propper image file name.
