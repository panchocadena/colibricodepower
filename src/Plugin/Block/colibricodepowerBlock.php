<?php
namespace Drupal\colibricodepower\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\file\Entity\File;

/**
 * Provides a 'Colibricode Power' Block.
 *
 * @Block(
 *   id = "colibricodepower",
 *   admin_label = @Translation("colibricodepower"),
 *   category = @Translation("Custom"),
 * )
 */
class colibricodepowerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#markup' => colibricodepower(),
    );
  }

}

function colibricodepower() {
  global $base_path;
  $themepath = $base_path.drupal_get_path('theme','theme_name');
  $img_path = $themepath."modules/custom/colibricodepower/images/logo-colibricode-white.png";

    $url = "http://colibricode.com";
    $power = "<a href='$url' target='_blank'>power by </a>";
    $img =  "<a href='$url' target='_blank'><img src='$img_path' alt='Colibricode software'></a>";




   $output = $power . $img;

  return $output;
}
